var React = require('react')
  , Locale = require('../stores/Locale')

module.exports = function(callback) {
  return {
    getInitialState: function() {
      return callback(this)
    },
    componentWillMount: function() {
      Locale.addChangeListener(this._onChange)
    },
    componentWillUnmount: function() {
      Locale.removeChangeListener(this._onChange)
    },
    _onChange: function() {
      if (this.isMounted()) {
        this.setState(callback(this))
      }
    }
  }
}
