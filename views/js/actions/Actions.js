var Constants = require('../constants/Constants'),
    Dispatcher = require('../dispatchers/Dispatcher')

module.exports = {
  setLanguage: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.LANGUAGE,
      item: item
    })
  }
}
