'use strict'

var React = require('react')
  , Header = require('./parts/Header')
  , Footer = require('./parts/Footer')

module.exports = React.createClass({
  render: function() {
    return (
      <div id="template" className="grid template">
        <Header/>
        {this.props.children}
        <Footer/>
      </div>
    )
  }
})
