'use strict'

var React     = require('react')
  , MenuBar   = require('../menus/MenuBar')
  , HeaderTop = require('../menus/HeaderTop')

module.exports = React.createClass({
  render: function() {
    return (
      <div id="header" className="header">
        <HeaderTop/>
        <MenuBar/>
      </div>
    )
  }
})
