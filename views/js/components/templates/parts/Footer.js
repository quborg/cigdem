'use strict'

var React       = require('react')
  , Copyright   = require('../blocs/Copyright')
  , FooterTop   = require('../menus/FooterTop')
  , FooterMenus = require('../menus/FooterMenus')
import { Nav } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <div id="footer" className="footer">
        <FooterTop/>
        <FooterMenus/>
        <Copyright/>
      </div>
    )
  }
})
