'use strict'

var React = require('react')
  , TxtIcon   = require('./TxtIcon')
  , Locale  = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    let P = this.state.locale.headerTop.phone
    return (
      <TxtIcon _class="phone" label={P.number} iconBefore={P.glyph}/>
    )
  }
})