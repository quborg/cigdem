'use strict'

var React = require('react')
  , Locale = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    return ( 
      <div id="copyright" className="copyright">
        <div className="container">
          {this.state.locale.footer.copyright}
        </div> 
      </div>
    )
  }
})
