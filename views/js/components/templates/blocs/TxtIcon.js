'use strict'

var React = require('react')

module.exports = React.createClass({
  render: function() {
    let B           = this.props
      , classes     = B._class ? 'box ' + B._class : 'box'
      , iconAfter   = B.iconAfter ? (<img src={B.iconAfter}/>) : ''
      , iconBefore  = B.iconBefore ? (<img src={B.iconBefore}/>) : ''
    return (
      <span className={classes}>
        {iconBefore}
        <span>{B.label}</span> 
        {iconAfter}
      </span>
    )
  }
})