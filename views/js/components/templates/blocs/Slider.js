'use strict'

var React = require('react')
  , Locale = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import { Carousel, CarouselItem } from 'react-bootstrap'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    return ( 
      <div id="carousel" className="carousel-cover">
        <Carousel indicators={false} controls={false} className="container">
          {
            this.state.locale.slider.map( (s,i) => {
              return (
                <CarouselItem key={i}>
                  <div className="carousel-caption">
                    <p className="txt-top">{s.txtTop}</p>
                    <p className="txt-bold">{s.txtBold}</p>
                  </div>
                </CarouselItem>
              )
            })
          }
        </Carousel>
      </div>
    )
  }
})
