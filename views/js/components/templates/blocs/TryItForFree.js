'use strict'

var React = require('react')
  , Locale = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import FlatButton from 'material-ui/lib/flat-button'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    return ( 
      <div className={(this.props._class||'')+"btn-on-row try-it-for-free"}>
        <FlatButton linkButton={true} href="#" label={this.state.locale.menuBar.tryItForFree}/>
      </div> 
    )
  }
})
