'use strict'

var $             = require('jquery')
  , React         = require('react')
  , TryItForFree  = require('../blocs/TryItForFree')
  , Locale        = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import { Row, Col } from 'react-bootstrap'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    let text = this.state.locale.footer.footerTop
    return (
      <div id="footer-top" className="footer-top">
        <div className="container">
          <Row>
            <Col xs={12}>
              <div className="text-footer-top ftelmt">{text.text1} <strong>{text.strong}</strong> {text.text2}</div>
              <TryItForFree _class="ftelmt "/>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
})
