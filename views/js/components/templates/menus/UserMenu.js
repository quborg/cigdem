'use strict'

var React         = require('react')
  , TxtIcon       = require('../blocs/TxtIcon')
  , Locale        = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import Menu from 'material-ui/lib/menus/menu'
import MenuItem from 'material-ui/lib/menus/menu-item'
import { browserHistory } from 'react-router'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  transition: function(url) { browserHistory.push(url) },
  render: function() {
    return (
      <Menu id="user-menu" className="user-menu">
        {
          this.state.locale.headerTop.userMenu.map( 
            (l,i) => {
              let img = (<img src={l.glyph}/>)
              if(l.class=='separator')
                return ( <span key={i} className="separator"></span> )
              else
                return ( <MenuItem key={i} className="user-menu-item" primaryText={l.title} leftIcon={img} onClick={this.transition.bind(null,l.to)}/> )
            }
          )
        }
      </Menu>
    )
  }
})
