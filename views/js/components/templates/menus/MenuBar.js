'use strict'

var React         = require('react')
  , TryItForFree  = require('../blocs/TryItForFree')
  , Locale        = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import { Navbar, Nav, Row, Col } from 'react-bootstrap'
import { IndexLink, Link, browserHistory } from 'react-router'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    return (
      <Navbar id="main-menu" className="container-fluid main-menu">
        <Row>
          <Col xs={3}>
            <div className="brand">
              <IndexLink className="logo" to="/">
                <img src="/images/logo.png"/>
              </IndexLink>
            </div>
          </Col>
          <Col xs={7}>
            <Nav>
              { 
                this.state.locale.menuBar.mainMenu.map( (l,k) => {
                  if(l.to=='/')
                    return ( <li key={k}><IndexLink to={l.to} activeClassName="active">{l.title}</IndexLink></li> )
                  else
                    return ( <li key={k}><Link to={l.to} activeClassName="active">{l.title}</Link></li> )
                })
              }
            </Nav>
          </Col>
          <Col xs={2}>
            <TryItForFree/>
          </Col>
        </Row>
      </Navbar>
    )
  }
})
