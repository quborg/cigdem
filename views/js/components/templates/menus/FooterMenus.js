'use strict'

var React         = require('react')
  , Locale        = require('../../../stores/Locale')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import { Row, Col } from 'react-bootstrap'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    return (
      <div id="footer-menus" className="footer-menus">
        <div className="container">
          <Row>
            { 
              this.state.locale.footer.menus.map( (m,i) => {
                return (
                  <Col key={i} xs={15} className={'menu-'+m.class}>
                    <div className="footer-menu-title">{m.title}</div>
                    <ul className="footer-menu-list ">
                      {
                        m.list.map( (l,j) => {
                          if (m.class !== 'social')
                            return ( <li key={j}><a href={l.to}>{l.title}</a></li> )
                          else
                            return ( <li key={j}><a href={l.to}><img src={l.src}/></a></li> )
                        })
                      }
                    </ul>
                  </Col>
                )
              })
            }
          </Row>
        </div>
      </div>
    )
  }
})
