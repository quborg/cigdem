'use strict'

var React         = require('react')
  , Phone         = require('../blocs/Phone')
  , BuyNow        = require('../blocs/BuyNow')
  , UserMenu      = require('./UserMenu')
  , LanguageMenu  = require('./LanguageMenu')
import { Row, Col } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <div id="header-top" className="header-top container-fluid">
        <div className="container">
          <Row>
            <Col xs={2}>
              <LanguageMenu/>
            </Col>
            <Col xs={2} xsOffset={3}>
              <Phone/>
            </Col>
            <Col xs={5} className="col-user-menu">
              <UserMenu/>
              <BuyNow/>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
})
