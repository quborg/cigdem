'use strict'

var React         = require('react')
  , TxtIcon       = require('../blocs/TxtIcon')
  , Locale        = require('../../../stores/Locale')
  , Actions       = require('../../../actions/Actions')
  , LocaleMixins  = require('../../../mixins/LocaleMixins')
import DropDownMenu from 'material-ui/lib/DropDownMenu'
import MenuItem     from 'material-ui/lib/menus/menu-item'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  getInitialState: function() { return { value: 0 } },
  handleChange: function(e, i, v) {
    this.setState({value:i})
    i == 0 ? Actions.setLanguage('tr') : Actions.setLanguage('en')
  },
  render: function() {
    return (
      <DropDownMenu id="lang-menu" value={this.state.value} onChange={this.handleChange} className="lang-menu">
        { 
          this.state.locale.headerTop.langMenu.map( (l,i) => {
            let item = <TxtIcon _class="lang" label={l.title} iconAfter={l.glyph}/>
            return ( <MenuItem key={i} value={i} label={item} primaryText={l.title}/> )
          })
        }
      </DropDownMenu>
    )
  }
})
