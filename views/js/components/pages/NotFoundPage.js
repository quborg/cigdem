'use strict'

var React = require('react')
  , Locale = require('../../stores/Locale')
  , LocaleMixins  = require('../../mixins/LocaleMixins')
import { Row, Col } from 'react-bootstrap'

function getLocale() { return { locale: Locale.getLocale() } }

module.exports = React.createClass({
  mixins: [LocaleMixins(getLocale)],
  render: function() {
    return (
      <Row id="not-found" className="container not-found">
        <Col xs={12} className="text-center">
          <p>{this.state.locale.pages.notFound}</p>
        </Col>
      </Row>
    )
  }
})
