'use strict'

var React   = require('react')
  , Slider  = require('../templates/blocs/Slider')
import { Row, Col } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <div id="home">
        <Slider/>
        <div className="home-content container">
          <Row className="row-1">
            <Col xs={7}>
              <h1>Pencerenizden hangi şehri gördüğünüz fark etmez!</h1>
              <p>Propars ile tüm ürünleriniz sadece birkaç dakikada dünyanın en.Propars ile tüm ürünleriniz sadece birkaç dakikada dünyanın en.Propars ile tüm ürünleriniz sadece birkaç dakikada dünyanın en </p>
            </Col>
            <Col xs={4}>
              <img src="/images/globe.png"/>
            </Col>
          </Row>
          <Row className="row-2">
            <Col xs={12}>
              <h2>Türkiye'nin ve dünyanın en popüler pazarlarında satış yapın</h2>
            </Col>
            <Col xs={12}>
              <img src="/images/brands.png"/>
            </Col>
          </Row>
          <Row className="row-3">
            <Col xs={7}>
              <h1>İnternet bağlantınız var mı? Öyleyse kontrol sizde.</h1>
              <p>Propars ile internete bağlanabilen herhangi bir cihazdan işletm. Propars ile internete bağlanabilen herhangi bir cihazdan işletm</p>
            </Col>
            <Col xs={5}>
              <img src="/images/zoomer.png"/>
            </Col>
          </Row>
          <Row className="row-4">
            <Col xs={5} className="text-center">
              <h1>Lorem Ipsum</h1>
              <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod temLorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod temLorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tem</p>
              <a href="#"><i>Read More</i></a>
            </Col>
            <Col xs={2} className="text-center">|</Col>
            <Col xs={5} className="text-center">
              <h1>Lorem Ipsum</h1>
              <p>Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod temLorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod temLorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tem</p>
              <a href="#"><i>Read More</i></a>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
})
