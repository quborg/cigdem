module.exports = {

  menuBar: {
    mainMenu: [
      {
        sys: "_1",
        title: "Home",
        to: "/"
      },
      {
        sys: "_2",
        title: "Propars ",
        to: "/propars"
      },
      {
        sys: "_3",
        title: "References",
        to: "/references"
      },
      {
        sys: "_4",
        title: "Pricing",
        to: "/pricing"
      },
      {
        sys: "_5",
        title: "Blog",
        to: "/blog"
      },
      {
        sys: "_6",
        title: "Contact Us",
        to: "/contact-us"
      }
    ],
    tryItForFree: 'Try It For Free'
  },

  headerTop: {
    langMenu: [
      {
        code: "tr",
        title: "Türkçe",
        glyph: "/images/icons/tr.png"
      },
      {
        code: "en",
        title: "English",
        glyph: "/images/icons/uk.png"
      }
    ],
    phone: {
      number: "0212 691 60 99",
      glyph: "/images/icons/phone.png"
    },
    buyNow: 'Buy Now',
    userMenu: [
      {
        sys: "_1",
        title: "Log In",
        class: "user-menu-item",
        to: "/#",
        glyph: "/images/icons/login.png"
      },
      {
        class: "separator"
      },
      {
        sys: "_2",
        title: "Help Center",
        class: "user-menu-item",
        to: "/#",
        glyph: "/images/icons/help-center.png"
      }
    ]
  },

  slider: [
    {
      txtTop: "Kepenk açmadan siftah yapan dükkanların çağındayız.",
      txtBold: "Propars'la zamanı yakalayın."
    },
    {
      txtTop: "Kepenk açmadan siftah yapan dükkanların çağındayız.",
      txtBold: "Propars'la zamanı yakalayın."
    },
    {
      txtTop: "Kepenk açmadan siftah yapan dükkanların çağındayız.",
      txtBold: "Propars'la zamanı yakalayın."
    }
  ],

  footer: {
    footerTop: {
      text1: "30 gün boyunca",
      strong: "Propars‘ı",
      text2: "ücretsiz deneyin!"
    },
    menus: [
      {
        title: 'Propars',
        class: 'propars',
        list: [
          { 
            title: 'Features',
            to: '#'
          },
          { 
            title: 'Smart Tools',
            to: '#'
          },
          { 
            title: 'E-Commerce Integrations',
            to: '#'
          },
          { 
            title: 'Propars API',
            to: '#'
          }
        ]
      },
      {
        title: 'Integrations',
        class: 'integrations',
        list: [
          { 
            title: 'Amazon.com',
            to: '#'
          },
          { 
            title: 'Ebay.com',
            to: '#'
          },
          { 
            title: 'GittiGidiyor.com',
            to: '#'
          },
          { 
            title: 'N11.com',
            to: '#'
          },
          { 
            title: 'Sanalpazar.com',
            to: '#'
          },
          { 
            title: 'Etsy.com',
            to: '#'
          }
        ]
      },
      {
        title: 'Help & Support',
        class: 'help-support',
        list: [
          { 
            title: '0212 691 60 99',
            to: '#'
          },
          { 
            title: 'Help Center',
            to: '#'
          },
          { 
            title: 'FAQ',
            to: '#'
          },
          { 
            title: 'User Manual',
            to: '#'
          },
          { 
            title: 'Terms Of Use',
            to: '#'
          },
          { 
            title: 'Privacy Policy',
            to: '#'
          }
        ]
      },
      {
        title: 'Topluluk',
        class: 'community',
        list: [
          { 
            title: 'Propars Blog',
            to: '#'
          },
          { 
            title: 'Propars Forum',
            to: '#'
          },
          { 
            title: 'Careers',
            to: '#'
          }
        ]
      },
      {
        title: 'Contact',
        class: 'contact',
        list: [
          { 
            title: '0212 691 60 99',
            to: '#'
          },
          { 
            title: 'Contact Us',
            to: '#'
          }
        ]
      },
      {
        title: 'Follow Us',
        class: 'social',
        list: [
          { 
            src: '/images/social/facebook.png',
            to: '#'
          },
          { 
            src: '/images/social/twitter.png',
            to: '#'
          },
          { 
            src: '/images/social/pinterest.png',
            to: '#'
          },
          { 
            src: '/images/social/instagram.png',
            to: '#'
          },
          { 
            src: '/images/social/googleplus.png',
            to: '#'
          }
        ]
      }
    ],
    copyright: "Copyright © Propars 2014-2015, Her Hakkı Saklıdır."
  },

  pages: {
    notFound: "PAGE NOT FOUND"
  }

}
