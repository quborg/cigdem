module.exports = {

  menuBar: {
    mainMenu: [
      {
        sys: "_1",
        title: "Ana Sayfa",
        to: "/"
      },
      {
        sys: "_2",
        title: "Propars'ı Tanıyın",
        to: "/propars"
      },
      {
        sys: "_3",
        title: "Referanslar",
        to: "/references"
      },
      {
        sys: "_4",
        title: "Fiyatlar",
        to: "/pricing"
      },
      {
        sys: "_5",
        title: "Blog",
        to: "/blog"
      },
      {
        sys: "_6",
        title: "İletişim",
        to: "/contact-us"
      }
    ],
    tryItForFree: 'Ücretsiz Deneyin'
  },

  headerTop: {
    langMenu: [
      {
        code: "tr",
        title: "Türkçe",
        glyph: "/images/icons/tr.png"
      },
      {
        code: "en",
        title: "English",
        glyph: "/images/icons/uk.png"
      }
    ],
    phone: {
      number: "0212 691 60 99",
      glyph: "/images/icons/phone.png"
    },
    buyNow: 'SATIN AL',
    userMenu: [
      {
        sys: "_1",
        title: "Giriş Yap",
        class: "user-menu-item",
        to: "/#",
        glyph: "/images/icons/login.png"
      },
      {
        class: "separator"
      },
      {
        sys: "_2",
        title: "Yardım Merkezi",
        class: "user-menu-item",
        to: "/#",
        glyph: "/images/icons/help-center.png"
      }
    ]
  },

  slider: [
    {
      txtTop: "Kepenk açmadan siftah yapan dükkanların çağındayız.",
      txtBold: "Propars'la zamanı yakalayın."
    },
    {
      txtTop: "Kepenk açmadan siftah yapan dükkanların çağındayız.",
      txtBold: "Propars'la zamanı yakalayın."
    },
    {
      txtTop: "Kepenk açmadan siftah yapan dükkanların çağındayız.",
      txtBold: "Propars'la zamanı yakalayın."
    }
  ],

  footer: {
    footerTop: {
      text1: "30 gün boyunca",
      strong: "Propars‘ı",
      text2: "ücretsiz deneyin!"
    },
    menus: [
      {
        title: 'Propars',
        class: 'propars',
        list: [
          { 
            title: 'Özellikler',
            to: "#"
          },
          { 
            title: 'Kullanım Kolaylıkları',
            to: "#"
          },
          { 
            title: 'Entegrasyonlar',
            to: "#"
          },
          { 
            title: 'Propars Apı',
            to: "#"
          }
        ]
      },
      {
        title: 'Entegrasyonlar',
        class: 'integration',
        list: [
          { 
            title: 'Amazon.com',
            to: '#' 
          },
          { 
            title: 'Ebay.com',
            to: '#' 
          },
          { 
            title: 'GittiGidiyor.com',
            to: '#' 
          },
          { 
            title: 'N11.com',
            to: '#' 
          },
          { 
            title: 'Sanalpazar.com',
            to: '#' 
          },
          { 
            title: 'Etsy.com',
            to: '#' 
          }
        ]
      },
      {
        title: 'Yardım & Destek',
        class: 'help-support',
        list: [
          { 
            title: '0212 691 60 99',
            to: '#' 
          },
          { 
            title: 'Yardım Merkezi',
            to: '#' 
          },
          { 
            title: 'Sıkça Sorulan Sorular',
            to: '#' 
          },
          { 
            title: 'Kullanım Kılavuzu',
            to: '#' 
          },
          { 
            title: 'Kullanım Koşulları',
            to: '#' 
          },
          { 
            title: 'Gizlilik Politikası',
            to: '#' 
          }
        ]
      },
      {
        title: 'Topluluk',
        class: 'community',
        list: [
          { 
            title: 'Propars Blog',
            to: '#'
          },
          { 
            title: 'Propars Forum',
            to: '#'
          },
          { 
            title: 'Kariyer',
            to: '#'
          }
        ]
      },
      {
        title: 'İletişim',
        class: 'contact',
        list: [
          { 
            title: '0212 691 60 99',
            to: '#'
          },
          { 
            title: 'Bize Ulaşın',
            to: '#'
          }
        ]
      },
      {
        title: 'Bizi Takip Edin',
        class: 'social',
        list: [
          { 
            src: '/images/social/facebook.png',
            to: '#'
          },
          { 
            src: '/images/social/twitter.png',
            to: '#'
          },
          { 
            src: '/images/social/pinterest.png',
            to: '#'
          },
          { 
            src: '/images/social/instagram.png',
            to: '#'
          },
          { 
            src: '/images/social/googleplus.png',
            to: '#'
          }
        ]
      }
    ],
    copyright: "Copyright © Propars 2014-2015, Her Hakkı Saklıdır."
  },

  pages: {
    notFound: "SAYFA BULUNAMADI"
  }

}
