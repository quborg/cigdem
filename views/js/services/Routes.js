'use strict'

import { Router, Route, IndexRoute, browserHistory } from 'react-router'

var React         = require('react')
  , Home          = require('../components/pages/Home')
  , Propars       = require('../components/pages/Propars')
  , Template      = require('../components/templates/Template')
  , NotFoundPage  = require('../components/pages/NotFoundPage')

module.exports = (
  <Router history={browserHistory}>
    <Route path="/" component={Template}>
      <IndexRoute component={Home}/>
      <Route path="/propars" component={Propars}/>
      <Route path="*" component={NotFoundPage}/>
    </Route>
  </Router>
)
