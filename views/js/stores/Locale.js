'use strict'

var CHANGE_EVENT = 'change'
  , assign = require('object-assign')
  , EN = require('../services/locales/EN.js')
  , TR = require('../services/locales/TR.js')
  , Constants = require('../constants/Constants')
  , EventEmitter = require('events').EventEmitter
  , Dispatcher = require('../dispatchers/Dispatcher')
  , Locale = TR

var AppStore = assign(EventEmitter.prototype.setMaxListeners(20), {
  emitChange: function() {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback)
  },
  getLocale: function() {
    return Locale
  },
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action
    switch (action.actionType){ 
      case Constants.LANGUAGE: 
        Locale = action.item == 'en' ? EN : TR
        break;
    }
    AppStore.emitChange()
    return true
  })
})

module.exports = AppStore
