'use strict'

var React     = require('react')
  , ReactDOM  = require('react-dom')
  , Routes    = require('./services/Routes')
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()

ReactDOM.render(Routes, document.getElementById('main'))
