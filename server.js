'use strict'

var express = require('express')
  , path = require('path')
  , app = express()

app.use(express.static(path.join(__dirname, 'assets')))

// Routes
app.get('/*', function(req,res){
  res.sendFile(__dirname + '/assets/index.html')
})

// Route not found -- Set 404
// app.get('*', function(req, res) {
//   res.status(404).send('route : Sorry this page does not accessible!')
// })

app.listen(3030, function(){
  console.log('Server is Up and Running at Port : 3030\nTunned on port 3000 by Gulp Sync')
})