'use strict'
var gulp        = require('gulp')
  , gutil       = require('gulp-util')
  , browserify  = require('browserify')
  , browserSync = require('browser-sync')
  , source      = require('vinyl-source-stream')

// Browserify Compile, Babel Transform & copy to assets/
/*
** JS
*/
gulp.task('browserify', function() {
  gutil.log('Compiling JS....')
  browserify('views/js/main.js')
    .transform('babelify', {presets: ["es2015", "react"]})
    .bundle()
    .on('error', function (err) {
        gutil.log(err.message)
        browserSync.notify("Browserify Error!")
        this.emit("end")
      })
    .pipe(source('main.js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(browserSync.stream({once: true}))
})

// Copy assets/
/*
** HTML
*/
gulp.task('html', function() {
  gulp.src('views/**/*.html')
    .pipe(gulp.dest('assets'))
})
/*
** IMAGES
*/
gulp.task('images', function() {
  gulp.src('views/images/**/*.*')
    .pipe(gulp.dest('assets/images'))
})
/*
** styles
*/
gulp.task('styles', function() {
  gulp.src('views/styles/**/*.css')
    .pipe(gulp.dest('assets/styles'))
})

// Browser LiveReload -- Development
/*
** @proxy : ExpressJS
** @files : assets
*/
gulp.task('sync', function() {
  browserSync.init({
    proxy: {
      target: 'localhost:3030'
    },
    files: ['./assets'],
    port: 3000
  })
})

// Gulp Default Start Routine
gulp.task('default',['browserify','html','images','styles','sync'], function() {
  gulp.watch('views/**/*.html', ['html'], function() {browserSync.reload()} )
  gulp.watch('views/styles/**/*.css', ['styles'], function() {browserSync.reload()} )
  gulp.watch('views/images/**/*.*', ['images'], function() {browserSync.reload()} )
  gulp.watch('views/js/**/*.*', ['browserify'], function() {browserSync.reload()} )
})
